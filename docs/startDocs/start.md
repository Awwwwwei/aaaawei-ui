# 快速开始

#### 安装组件库

```bash
npm i aaaawei-ui
```

#### 使用组件库
> 在 main.js 中引入组件库

```javascript
//全部引入
import 'aaaawei-ui/dist/css/index.css';
import AUI from 'aaaawei-ui';
Vue.use(AUI);

// 按需引用
import 'aaaawei-ui/dist/css/demo.css';
import {Demo} from 'aaaawei-ui';
Vue.use(Demo);
```