# Switch
开关组件

### 基本用法
::: demo 由type属性来选择tag的类型
```html
<template>
  <div>
    <a-switch />
  </div>
</template>
```
:::

### 基本用法
::: demo 由type属性来选择tag的类型
```html
<template>
  <div>
    <a-switch activeColor="red" inactiveColor="blue"/>
  </div>
</template>
```
:::

### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| v-model | 双向绑定 | Boolean | false | false |
| name | 名称 | String | false | - |
| activeColor | 激活时的颜色 | String | false | - |
| inactiveColor | 不激活时的颜色 | String | false | - |



### Events
| 事件名称 | 说明 | 回调参数 |
| ------ | ----- | ----- |
| handleClick | 点击事件 | - | 
