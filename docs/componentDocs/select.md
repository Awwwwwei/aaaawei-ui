# Select
选择器

### 基础用法 
::: demo v-model的值为当前被选中的a-select的 value 属性值
```html
<template>
  <div>
    <a-select placeholder='请输入内容' v-model="value" :list=list />
  </div>
</template>
<script>
export default {
  data() {
    return {
        list:[
                { value:1,label:'吃饭'},
                { value:2,label:'睡觉'},
                { value:3,label:'洗澡'},
            ],
        value:''
    }
  }
}
</script>
```
:::

### 有禁用选项 
::: demo v-model的值为当前被选中的a-select的 value 属性值
```html
<template>
  <div>
    <a-select placeholder='请输入内容' v-model="value" :list=list disabled />
  </div>
</template>
<script>
export default {
  data() {
    return {
        list:[
                { value:1,label:'吃饭'},
                { value:2,label:'睡觉'},
                { value:3,label:'洗澡'},
            ],
        value:''
    }
  }
}
</script>
```
:::




### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| placeholder | 占位符 | String | false | false |
| list | 选择框需要遍历的数组 | Array | false | - |
| disabled | 是否禁用 | Boolean | false | false |


### Events
| 事件名称 | 说明 | 回调参数 |
| ------ | ----- | ----- |
| clickValue | 点击选项事件 | - | 