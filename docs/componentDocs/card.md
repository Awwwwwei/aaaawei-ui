# Card
卡片组件

### 基础用法
::: demo 使用imgSrc可以来定义图片内容、summary可以定义图片的标题。
```html
<template>
  <a-card imgSrc="https://aaaaawei.oss-cn-beijing.aliyuncs.com/202201.jpg" summary="图片标题" />
</template>
```
:::

### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| width | 卡片的宽度 | Number | false | - |
| imgSrc | 图片资源地址 | String | true | - |
| imgHeight | 图片高度 | Number | false | - |
| summary | 卡片概要 | String/Slog | flase | - |
| footer | 卡片底部 | Slot | false | - | 
