# Dialog
对话框组件
在保留当前页面状态的情况下，告知用户并承载相关操作。

### 基础用法
Dialog 弹出一个对话框，适合需要定制性更大的场景。
::: demo 使用type、round和circle属性来定义 Button 的样式。
```html
<template>
    <div>
        <a-button @handleClick="showDialog" btnMsg="展示对话框" ></a-button>
        <a-dialog :visible.sync="dialogShow"></a-dialog>
    </div>
</template>
<script>
export default {
  data() {
    return {
        dialogShow:false
    }
  },
  methods: {
    showDialog(){
        this.dialogShow=!this.dialogShow
    }
  },
}
</script>
```
:::

### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| visible | 是否显示 Dialog，支持 .sync 修饰符 | boolean | false | false |
| title | Dialog 的标题 | String | - | - |
| top | Dialog CSS 中的 margin-top 值 | String | - | 15vh |
| width | Dialog 的宽度 | string | - | 50% |


### Events
| 事件名称 | 说明 | 回调参数 |
| ------ | ----- | ----- |
| handleClose | Dialog 关闭的回调 | - | 

