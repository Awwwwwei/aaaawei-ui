# Input
输入框组件
通过鼠标或键盘输入字符

### 基础用法
::: demo 
```html
<template>
  <div>
    <a-input placeholder='请输入内容'/>
  </div>
</template>
<script>
export default {
  data() {
    return {
     
    }
  }
}
</script>
```
:::

### 禁用状态
::: demo 通过 disabled 属性指定是否禁用 input 组件
```html
<template>
  <div>
    <a-input placeholder='请输入内容' disabled/>
  </div>
</template>
<script>
export default {
  data() {
    return {
     
    }
  }
}
</script>
```
:::

### 可清空 
::: demo 使用clearable属性即可得到一个可清空的输入框
```html
<template>
  <div>
    <a-input placeholder='请输入内容' clearable v-model='input'/>
  </div>
</template>
<script>
export default {
  data() {
    return {
        input:''
    }
  }
}
</script>
```
:::

### 密码框  
::: demo 使用show-password属性即可得到一个可切换显示隐藏的密码框
```html
<template>
  <div>
    <a-input placeholder='请输入内容' type='password' showPassword />
  </div>
</template>
```
:::


### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| placeholder | 占位符 | String | false | false |
| type | 文本框类型（text/password） | String | false | text |
| clearable | 是否显示清空按钮 | Boolean | false | false |
| showPassword | 是否显示密码切换按钮 | Boolean | false | false |
| disabled | 是否禁用 | Boolean | false | false |
| name | name属性 | String | false | 无 |


### Events
| 事件名称 | 说明 | 回调参数 |
| ------ | ----- | ----- |
| blur | 失去焦点事件 | - | 
| change | 内容改变事件 | - | 