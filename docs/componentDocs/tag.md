# Tag
标签组件

### 基础用法
::: demo 由type属性来选择tag的类型
```html
<template>
  <div>
    <a-tag type='primary' />
    <a-tag type='success' />
    <a-tag type='warning' />
    <a-tag type='danger' />
    <a-tag type='info' />
  </div>
</template>
<script>
export default {
  data() {
    return {
     
    }
  }
}
</script>
```
:::

### 可移除标签
::: demo 由type属性来选择tag的类型
```html
<template>
  <div>
    <a-tag type='primary' closable/>
    <a-tag type='success' closable/>
    <a-tag type='warning' closable/>
    <a-tag type='danger' closable/>
    <a-tag type='info' closable/>
  </div>
</template>
<script>
export default {
  data() {
    return {
     
    }
  }
}
</script>
```
:::

### 不同尺寸
::: demo 由type属性来选择tag的类型
```html
<template>
  <div>
    <a-tag type='primary' size="mini"/>
    <a-tag type='primary' size="small"/>
    <a-tag type='primary' size="medium"/>
  </div>
</template>
<script>
export default {
  data() {
    return {
     
    }
  }
}
</script>
```
:::


### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| type | 标签的类型primary/success/warning/danger/info | String | false | primary |
| round | 是否是圆角标签 | Boolean | false | false |
| disabled | 是否禁用标签 | Boolean | false | false |
| size | 标签的大小 | String | false | small |
| content | 标签文本内容 | String | false | 标签 |


### Events
| 事件名称 | 说明 | 回调参数 |
| ------ | ----- | ----- |
| handleClose | 点击关闭按钮事件 | - | 

