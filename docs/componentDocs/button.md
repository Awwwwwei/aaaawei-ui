# Button
按钮组件

### 基础用法
基础的按钮用法。
::: demo 使用type、round和circle属性来定义 Button 的样式。
```html
<template>
  <div class="main">
    <div>
        <a-button type='primary' btnMsg="主要按钮" />
        <a-button type='success' btnMsg="成功按钮" />
        <a-button type='info' btnMsg="信息按钮" />
        <a-button type='danger' btnMsg="警告按钮" />
    </div>
    <div class="footer">
        <a-button type='primary' btnMsg="主要按钮" round/>
        <a-button type='success' btnMsg="成功按钮" round/>
        <a-button type='info' btnMsg="信息按钮" round/>
        <a-button type='danger' btnMsg="警告按钮" round/>
    </div>
  </div>
</template>
<style>
.footer{
    margin-top:15px;
}
</style>
```
:::

### 禁用状态
按钮不可用状态。
::: demo 你可以使用disabled属性来定义按钮是否可用，它接受一个Boolean值。
```html
<template>
  <div class="main">
        <a-button type='primary' btnMsg="主要按钮" disabled/>
        <a-button type='success' btnMsg="成功按钮" disabled/>
        <a-button type='info' btnMsg="信息按钮" disabled/>
        <a-button type='danger' btnMsg="警告按钮" disabled/>
  </div>
</template>
```
:::

### 不同尺寸
Button 组件提供三种尺寸，可以在不同场景下选择合适的按钮尺寸。
::: demo 额外的尺寸：medium、small、mini，通过设置size属性来配置它们。
```html
<template>
  <div>
    <a-button type='primary' btnMsg="超小按钮" size="mini"/>
    <a-button type='primary' btnMsg="小型按钮" size="small"/>
    <a-button type='primary' btnMsg="中等按钮" size="medium"/>
  </div>
</template>
<script>
export default {
  data() {
    return {
     
    }
  }
}
</script>
<style>
</style>
```
:::

### Attributes
| 参数 | 说明 | 类型 | 是否必要 | 默认值 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| type | 按钮的类型primary/success/warning/danger/info | String | false | primary |
| round | 是否是圆角按钮 | Boolean | false | false |
| circle | 是否是圆形按钮 | Boolean | false | false |
| disabled | 是否禁用按钮 | Boolean | false | false |
| size | 按钮的大小 | String | false | small |
| btnMsg | 按钮文本内容 | String | false | 按钮 |


### Events
| 事件名称 | 说明 | 回调参数 |
| ------ | ----- | ----- |
| handleClick | 点击事件 | - | 

