module.exports = {
  title: 'aaaawei-ui',
  base: '/aaui-using-documents/',
  plugins: ['demo-container'],
  themeConfig: {
    sidebar: [
      {
        title: '安装',
        collapsable: false,
        children: [
          {
            title: '快速开始',
            path: '/startDocs/start',
          }
        ]
      },
      {
        title: '组件',
        collapsable: false,
        children: [
          {
            title: 'Card卡片',
            path: '/componentDocs/card'
          },
          {
            title: 'Button按钮',
            path: '/componentDocs/button'
          },
          {
            title: 'Input输入框',
            path: '/componentDocs/input'
          },
          {
            title: 'Switch开关',
            path: '/componentDocs/switch'
          },
          {
            title: 'Select选择器',
            path: '/componentDocs/select'
          },
          {
            title: 'Dialog对话框',
            path: '/componentDocs/dialog'
          },
          {
            title: 'Tag标签',
            path: '/componentDocs/tag'
          }
        ]
      },
    ],
    nav: [
      { text: '组件', link: '/componentDocs/card' },
      { text: 'Gitee', link: 'https://gitee.com/Awwwwwei/aaaawei-ui' }
    ]
  }
}