import Demo from "./demo";
import Card from "./card";
import Button from './button'
import Input from './input'
import Switch from './switch'
import Select from "./select";
import Dialog from "./dialog";
import Tag from "./tag";
const components = {
    Demo,
    Card,
    Button,
    Input,
    Switch,
    Select,
    Dialog,
    Tag
}

const install = function (Vue) {
    if (install.installed) return;
    Object.keys(components).forEach(key => {
        Vue.component(components[key].name, components[key])
    })
}

const API = {
    install
}

export default API