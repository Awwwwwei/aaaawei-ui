import Vue from 'vue'
import App from './App.vue'

// import 'aaaawei-ui/dist/css/index.css';
// import AUI from 'aaaawei-ui';
// Vue.use(AUI);

import '../components/css/popover.scss'
import Popover from '../components/lib/popover'
Vue.use(Popover)

import '../components/css/tag.scss'
import Tag from '../components/lib/tag'
Vue.use(Tag)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
