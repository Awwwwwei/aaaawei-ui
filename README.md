# awei-ui 组件库

### 快速开始

#### 1.安装组件库

```bash
npm i aaaawei-ui
```

#### 2.引用组件库
```javascript
// 全部引入
import 'aaaawei-ui/dist/css/index.css';
import AUI from 'aaaawei-ui';
Vue.use(AUI);

// 按需引用
import 'aaaawei-ui/dist/css/demo.css';
import {Demo} from 'aaaawei-ui';
Vue.use(Demo);
```

